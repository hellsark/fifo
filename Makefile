CXX=g++

ALL:one_producer_one_consumer

one_producer_one_consumer:one_producer_one_consumer.cpp
	${CXX} one_producer_one_consumer.cpp -lpthread -o one_producer_one_consumer

clean:
	rm one_producer_one_consumer