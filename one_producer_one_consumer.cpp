#define _GNU_SOURCE
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <sched.h>
//#define BIND_CORE 1
#define TEST_MAX 20000
#define smp_mb()asm volatile("dmb ish" ::: "memory")
#define smp_wmb()asm volatile("dmb ishst" ::: "memory")
#define smp_rmb()asm volatile("dmb ishld" ::: "memory")

#define _smp_mb() asm volatile("dmb sy" ::: "memory")
#define _smp_rmb() asm volatile("dmb ld" ::: "memory")
#define _smp_wmb() asm volatile("dmb st" ::: "memory")

typedef struct _RING_BUFFER_S{
  unsigned int in;
  unsigned int out;
  unsigned int mask; // mask + 1 == fifo's length
  unsigned int esize; // the size of single element
  int *members; // fifo buffer
}RING_BUFFER_S;
#define RING_BUFFER_SIZE 16
int r1, r2, r3;
RING_BUFFER_S *g_ring;

static int ring_buffer_push(RING_BUFFER_S *q, int member)
{
  if ((q->in - q->out) >= q->mask)
    {
       return -1;
    }
  q->members[q->in & q->mask] = member;
  q->in++;
  return 0;
}

static int ring_buffer_pop(RING_BUFFER_S *q)
{
  int member = 0;
  if (q->in == q->out) 
    {
       return -1;
    } 
  member = q->members[q->out & q->mask];
  q->out++;
  return member;
}

RING_BUFFER_S* ring_buffer_alloc(unsigned int size)
{
  RING_BUFFER_S* rb = (RING_BUFFER_S*)malloc(sizeof(RING_BUFFER_S));
  if (rb == NULL) {
    return NULL;
  }
  rb->in = 0;
  rb->out = 0;
  rb->esize = sizeof(int);
  rb->members = (int*)malloc(rb->esize * size);
  memset(rb->members,0,rb->esize * size);
  if (rb->members == NULL) {
    free(rb);
    return NULL;
  }
  rb->mask = size - 1;
  return rb;
}

void ring_buffer_free(RING_BUFFER_S* ring)
{
  free(ring->members);
  free(ring);
}

void *consumer(void*)
{
  #ifdef BIND_CORE
  cpu_set_t cpu_set;
  CPU_ZERO(&cpu_set);
  CPU_SET(1, &cpu_set);
  if (pthread_setaffinity_np(pthread_self(),sizeof(cpu_set),&cpu_set) < 0) {
    printf("error");
  }
  #endif

  // pop three element 1 2 3
  while (r1 == -1) {
    r1 = ring_buffer_pop(g_ring);
  }
  while (r2 == -1) {
    r2 = ring_buffer_pop(g_ring);
  }
  while (r3 == -1) {
    r3 = ring_buffer_pop(g_ring);
  }
}

void *producer(void*)
{
  #ifdef BIND_CORE
  cpu_set_t cpu_set;
  CPU_ZERO(&cpu_set);
  CPU_SET(127, &cpu_set);
  if (pthread_setaffinity_np(pthread_self(),sizeof(cpu_set),&cpu_set) < 0) {
    printf("error");
  }
  #endif

  // push three element 1 2 3
  ring_buffer_push(g_ring, 1);
  ring_buffer_push(g_ring, 2);
  ring_buffer_push(g_ring, 3);
}

int main(int argc, char **argv)
{
  pthread_t t1, t2;
  int err_count = 0;
  g_ring = ring_buffer_alloc(RING_BUFFER_SIZE);
  for (int i = 0; i < TEST_MAX; i++) {
    r1 = r2 = r3 = -1;
    pthread_create(&t1, NULL, consumer,NULL);
    pthread_create(&t2, NULL, producer,NULL);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    if (r1 != 1 || r2 != 2 || r3 !=3) {
      printf("r1 : %d, r2 : %d, r3 : %d\n", r1, r2, r3);
      err_count++;
    }
  }
  free(g_ring);
  printf("Test %d times, error happened %d times\n", TEST_MAX, err_count);
  return 0;
}